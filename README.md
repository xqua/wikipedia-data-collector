# Wikipedia Data Collector

This project is in collaboration with Omer and Rona about parsing Wiki data onto a pymongo or other style databse.

## Requirements
### MongoDB 
You will need a working instance of a MongoDB, there are three ways to acheive this:
<<<<<<< HEAD
1) (Not my reccommendation) you can install mongoDB on your local machine [following the guide](https://docs.mongodb.com/manual/installation/)
2) (My reccomendation for local non shared work) You can use docker to setup a mongoDB on your machine, first [install docker](https://docs.docker.com/get-docker/) and [docker-compose](https://docs.docker.com/compose/install/) then you can launch a mongoDB by navigating to the root of this folder and typing `sudo docker-compose up -d`
3) (Reccommended for shared work with others) You can use an online mongoDB instance on the Atlas cloud. It is free up to a point. First [setup a Mongo Cluster on Atlas](https://www.mongodb.com/atlas/database). Then, in the Network Access tab, make sure to add **0.0.0.0** as an external access address so that you can connect to the DB from any computer. 

### Python packages
- pymongo
=======

  1. (Not my recommendation) you can install mongoDB on your local machine [following the guide](https://docs.mongodb.com/manual/installation/)
  2. (My recomendation for local non shared work) You can use docker to setup a mongoDB on your machine, first [install docker](https://docs.docker.com/get-docker/) and [docker-compose](https://docs.docker.com/compose/install/) then you can launch a mongoDB by navigating to the root of this folder and typing `sudo docker-compose up -d`
  3. (Recommended for shared work with others) You can use an online mongoDB instance on the Atlas cloud. It is free up to a point (about one or two wiki articles will fit in it). So if you want to use this, you probably need to plan ahead for a 15$/month budget. First [setup a Mongo Cluster on Atlas](https://www.mongodb.com/atlas/database). Then, in the Network Access tab, make sure to add **0.0.0.0** as an external access address so that you can connect to the DB from any computer. Finally go on the Database tab click the Connect button on your cluster to get the connection URI.

### Python packages
- pymongo with the [srv] flag: `pip install "pymongo[srv]"`
- tqdm 
- numpy
- matplotlib
- seaborn
- pandas

## Usage
In order to collect data from wikipedia, you must first use the `WikipediaParser.ipynb` to collect data. First change the uri to the URI of Atlas or to None if you are using the docker mongoDB

Finally, change the article list to add all the articles you want from wikipedia and run the parser. When it is finished the data is saved in the database and can be accessed for further analysis which you can find a demo in the `WikipediaAnalysisDemo.ipynb`.

## Database format
The database is structured in three collection: articles, revisions, pageviews:

### articles

The schema of an article object looks like this:
```json
{
    '_id': Internal mongoDB identifier,
    'categories': The list of categories this article belongs to AT THE MOST RECENT REVISION,
    'contributors': The list of contributors for this article,
    'links': The list of outgoing links from this article AT THE MOST RECENT REVISION,
    'pageid': The unique internal wikipedia ID of the article,
    'title': The title of the article, 
    'anoncontributors': Count of anonymous contributors
}
```

## revisions

The schema of a revision object looks like this:
```json
{
    '_id': Internal mongoDB identifier,
    'user': The username who commited the edit, 
    'userid': The unique internal to wikipedia user identifier, 
    'timestamp': The date and time of the edit, 
    'size': The current size of the page after the revision, 
    'delta_size': The revision change in page size, 
    'comment': The comment by the editor on the edit, 
    'tags': The tags used as metadata to decorate the edit (Only available after 2018), 
    'title': The title of the article that the edit was done on (crossref to articles collection through this), 
    'pageid': The unique internal wikipedia ID of the article (crossref to articles collection through this), 
    'content': The content of the page after the edit was done, 
    'sections': The title of all the sections and subsections ordered in a recursive dictionary, 
    'toc': The table of content as a nested list, 
    'boxes': The list of all scripted tags and info boxes, 
    'citations': The list of all the citations of the article at that revision, 
    'files': The list of all the files inserted of the article at that revision, 
    'images': The list of all the image files inserted of the article at that revision, 
    'links': The list of all the outgoing links of the article at that revision, 
    'categories': The list of all the categories of the article at that revision, 
    'langs': The language of the article, 
    'is_revert': If this edit is a revert or a rollback, 
    'is_archiving': If this edit is archiving a Talk: section, 
    'is_protected': If this article at that revision is protected using a {{pp-XXX}} tag, 
    'scientific_score': The scientific score is defined as the number of citation with a DOI, PMID or PMC divided by the total number of citations.
}
```

## pageviews

The schema of a pageview object looks like this:
```json
{
    '_id': Internal mongoDB identifier,
    'article': The title of the article that the edit was done on (crossref to articles collection through this),
    'timestamp': The date for which the views number corresponds, 
    'views': The number of page view for that given date, 
    'granularity': Should be daily always but could be changed to monthly if need be
}
```
>>>>>>> 250b820dc9fb479c987a205f2104c252274d56e3
